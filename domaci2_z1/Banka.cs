﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci2_z1
{
    class Banka
    {

        string ime;
        int brojStedisa;
        Stedisa[] podaci;
        int brojBlokiranih;
        float ukupnoUplaceno;
        

       
        public void Ucitavanje()
        {
            Console.WriteLine("Unesite ime banke:");
            this.ime = Console.ReadLine();
            Console.WriteLine("Unesite broj stedisa banke:");
            int.TryParse(Console.ReadLine(), out this.brojStedisa);
            this.podaci = new Stedisa[this.brojStedisa];
            for(int i = 0; i < podaci.Length; i++)
            {
                Console.WriteLine($"Unestite podatke za {i + 1}. korisnika:");
                this.podaci[i] = new Stedisa();
                this.podaci[i].Ucitavanje();
                this.podaci[i].Prikazivanje();
                
            }
        }
        private Stedisa TrazenjeStedise(int x, out Stedisa y)
        {
            
            y = null;
            

            for(int i = 0; i < this.podaci.Length; i++)
            {
                if (this.podaci[i].BrojRacuna == x)
                {
                    Console.WriteLine($"Stedisa sa brojem racuna {x} pronadjen!");
                    this.podaci[i].Prikazivanje();
                    y = this.podaci[i];
                   
                }

            }
            if (y == null)
            {
                Console.WriteLine($"Ne postoji stedisa sa brojem racuna {x}");
            }
            return y;
            
        }
        public void Search()
        {
            Console.WriteLine("Uneti broj racuna za pretragu:");
            int.TryParse(Console.ReadLine(), out int x);
            TrazenjeStedise(x,out Stedisa y);
            PromenaStanja(ref y);
            
            
        }
        public void PromenaStanja(ref Stedisa x)
        {
            if(x.Status == Status.Aktivan)
            {
                x.DodavanjeNaStanje();
                x.OduzimanjeSaStanja();
            }
            else
            {
                x.DodavanjeNaStanje();
            }
        }
        public void PrikazBlokiranih()
        {
            Console.WriteLine("Spisak blokiranih:");
            foreach (Stedisa s in podaci)
            {
                if(s.Status == Status.Blokiran)
                {
                    this.brojBlokiranih++;
                    Console.WriteLine($"{s.BrojRacuna}");
                }
            }
            if(this.brojBlokiranih == 0)
            {
                Console.WriteLine("Nema korisnika sa blokiranim racunom");
            }
        }
        public void UkupnoUlozeno()
        {
            for(int i = 0; i < podaci.Length; i++)
            {
                this.ukupnoUplaceno += this.podaci[i].Stanje;
            }
            Console.WriteLine($"Ukupno stanje je {this.ukupnoUplaceno}");
        }
        
       



    }
}
