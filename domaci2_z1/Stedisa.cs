﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci2_z1
{
    class Stedisa
    {
        string ime;
        string prezime;
        float stanje = 0;
        int brojRacuna;
        private Status status;
        

        public void Ucitavanje()
        {
            Console.WriteLine("Ime:");
            this.ime = Console.ReadLine();
            Console.WriteLine("Prezime:");
            this.prezime = Console.ReadLine();
            Console.WriteLine("Broj racuna:");
            this.brojRacuna = int.Parse(Console.ReadLine());
            Console.WriteLine("Stanje:");
            this.stanje = float.Parse(Console.ReadLine());
            this.status = (this.stanje <= 0) ? Status.Blokiran : Status.Aktivan;

        }
        public int BrojRacuna
        {
            get
            {
                return this.brojRacuna;
            }
            set
            {
                this.brojRacuna = value;
            }
        }
        public float Stanje
        {
            get
            {
                return this.stanje;
            }
            set
            {
                this.stanje = value;
            }
        }
        public Status Status
        {
            get
            {
                return status;
            }
            set
            {
                this.status = value;
            }
        }

        public void Prikazivanje()
        {
            Console.WriteLine($"Korisnik: {this.ime} {this.prezime} {this.brojRacuna} {this.stanje} {this.status}");
        }

        public float DodavanjeNaStanje()
        {
            Console.WriteLine("Unesite sumu za uplatu: ");
            float.TryParse(Console.ReadLine(), out float x);
            this.stanje += x;
            if (this.status == Status.Aktivan)
            {
                Console.WriteLine($"Novo stanje na racunu je {this.stanje}");
            }
            if(this.status == Status.Blokiran)
            {
                this.status = Status.Aktivan;
                Console.WriteLine($"Novo stanje na racunu je {this.stanje} i status racuna je {this.status}");
            }
            return this.stanje;

        }

        public float OduzimanjeSaStanja()
        {
            float rez = 0;
            float y;
            Console.WriteLine("Unesite sumu za isplatu: ");
            float.TryParse(Console.ReadLine(), out y);
            if(this.status != Status.Blokiran && y < this.stanje)
            {
                this.stanje -= y;
                rez = this.stanje;
                Console.WriteLine($"Uzeta suma sa racuna je {y} a preostali iznos na racunu je {this.stanje}");
                return rez;
            }
            else if (this.status != Status.Blokiran && y >= this.stanje)
            {
                rez = this.stanje;
                this.stanje = 0;
                this.status = Status.Blokiran;
                Console.WriteLine($"Uzeta suma sa racuna je {rez}");
                Console.WriteLine($"Novo stanje na racunu je {this.stanje} i novi status racuna je {this.status}");
                return rez;

            }
            else if(this.status == Status.Blokiran)
            {
                Console.WriteLine($"Racun je {this.status} i nije moguce uzeti novac!");
            }
            
            return rez;


        }
    }
}
